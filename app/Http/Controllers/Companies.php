<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Imports\CompaniesImport;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use App\Models\Company;
class Companies extends Controller
{
    public function import(Request $request)
    {
         $request->validate([
            'import_file' => 'required|file|mimes:xls,xlsx'
        ]);

        $path = $request->file('import_file');
        $array = Excel::toArray(new CompaniesImport, $path);
        $data = $this->getData($array[1]);
        foreach ($data as $key => $tosave) {
            $company = Company::create($tosave);
        }
        $company->save();
        $result = Company::all();
        return response()->json($result, 200);
    }

    private function getData($array){
        $data = [];
        for ($i=0; $i < count($array); $i++) { 
            $currentCellFirst = 0;
            $currentCellSecond = 0;
            for ($j=0; $j <= 9; $j++) { 
                if($i>2 && $j>1){
                    if($j%2==0 && $currentCellFirst<$j){
                        $currentCellFirst = $j+2;
                        $company = $array[$i][1];
                        $date = Date::excelToDateTimeObject($array[2][$j])->format('d.m.Y');
                        $fact_forecast = ($j<6)?'fact':'forecast';
                        $index = $company.$date.$fact_forecast;
                        if(array_key_exists($index, $data)){
                            $data[$index]['qliq'] += $array[$i][$j];
                            $data[$index]['qoil'] += $array[$i][$currentCellFirst];
                        }else{
                            $data[$index]['company'] = $company;
                            $data[$index]['qliq'] = $array[$i][$j];
                            $data[$index]['qoil'] = $array[$i][$currentCellFirst];
                            $data[$index]['date'] = $date;
                            $data[$index]['fact_forecast'] = $fact_forecast;
                        }
                    }elseif($j%2==1 && $currentCellSecond<$j){
                        $currentCellSecond = $j+2;
                        $company = $array[$i][1];
                        $date = Date::excelToDateTimeObject($array[2][$j])->format('d.m.Y');
                        $fact_forecast = ($j<6)?'fact':'forecast';
                        $index = $company.$date.$fact_forecast;
                        if(array_key_exists($index, $data)){
                            $data[$index]['qliq'] += $array[$i][$j];
                            $data[$index]['qoil'] += $array[$i][$currentCellFirst];
                        }else{
                            $data[$index]['company'] = $array[$i][1];
                            $data[$index]['qliq'] = $array[$i][$j];
                            $data[$index]['qoil'] = $array[$i][$currentCellSecond];
                            $data[$index]['date'] = Date::excelToDateTimeObject($array[2][$j])->format('d.m.Y');
                            $data[$index]['fact_forecast'] = ($j<6)?'fact':'forecast';
                        }
                    }
                }
            }
        }
        return array_values($data);
    }

    public function getCompanies()
    {
        $result = Company::select('company')->groupByRaw('company')->orderByRaw('CHAR_LENGTH(company)')->orderBy('company', 'ASC')->get();
        return response()->json($result, 200);
    }
    public function getCompanyData(String $company)
    {
        $fact = Company::select('qliq','qoil')->where('company', '=', $company)->where('fact_forecast', '=', 'fact')->get();
        $forecast = Company::select('qliq','qoil')->where('company', '=', $company)->where('fact_forecast', '=', 'forecast')->get();
        return compact('fact','forecast');
    }
}
