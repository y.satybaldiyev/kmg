<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Companies;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('/companies/import', [Companies::class, 'import']);
Route::get('/companies/getCompanies', [Companies::class, 'getCompanies']);
Route::get('/companies/getCompanyData/{company}', [Companies::class, 'getCompanyData']);